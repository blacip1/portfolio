// eslint-disable-next-line import/no-extraneous-dependencies
import express from "express";
import readline from "readline";

const app = express();
const port = process.env.PORT || 5050; // Default port or 3000

// Serve files from the 'src' directory
app.use(express.static("src"));

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
  console.log("Press 'q' to quit the server.");

  // Create a readline interface
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  // Set a timeout of 120 seconds
  setTimeout(() => {
    console.log("Server timed out. Quitting.");
    process.exit(0);
  }, 120000);

  // Listen for the 'q' key to quit the server
  rl.on("line", (input) => {
    if (input.toLowerCase() === "q") {
      console.log(" Quitting server.");
      process.exit(0);
    }
  });
});
